import { Controller, Post, Body, Headers } from '@nestjs/common'
import { JobHook } from './events/job-hook.interface'
import { HoneycombService } from '../honeycomb/honeycomb.service'

// type Constructor<T> = { new (...args: any[]): T }

// export const GitlabHooksMap = {
//   'Job Hook': JobHook
// }

// function isJobHook(hook: GitlabHooks): hook is JobHook {
//   return hook.type === 'Job Hook'
// }

@Controller('/gitlab')
export class GitlabWebhookController {
  constructor(private apm: HoneycombService) {}

  @Post('/webhook')
  async webhook(
    @Headers('x-gitlab-event') eventType: string,
    @Body() event: JobHook,
  ) {
    console.log('')
    console.log('')
    console.log('')
    console.log('')
    console.log('')
    console.log('----------------------------------------------------------------------------------------------------')
    console.log(`  ${eventType}`)
    console.log('----------------------------------------------------------------------------------------------------')
    console.log('')

    console.log(event)

    if (eventType !== 'Job Hook') {
      return { response: 'ok' }
    }


    if ([ 'success', 'failed' ].indexOf(event.build_status) > -1) {
      const traceId = `Build (Pipeline: ${event.pipeline_id}, SHA:${event.commit.sha})`

      await this.apm.event('development-telemetry', {
        name: 'stage',
        serviceName: event.build_stage,
        spanId: event.build_stage,
        parentId: traceId,
        traceId,
      }, new Date(event.build_started_at))

      await this.apm.event('development-telemetry', {
        name: 'job',
        serviceName: event.build_stage,
        traceId,
        parentId: event.build_stage,
        spanId: event.build_name,
        status: event.build_status,
        durationMs: event.build_duration * 1000,
      }, new Date(event.build_started_at))
    }

    return { response: 'ok' }
  }
}
