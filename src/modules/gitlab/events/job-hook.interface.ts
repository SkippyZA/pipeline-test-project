export class JobHook {
  object_kind: string
  ref: string
  tag: boolean
  before_sha: string
  sha: string
  build_id: number
  build_name: string
  build_stage: string
  build_status: string
  build_started_at: null
  build_finished_at: null
  build_duration: number
  build_allow_failure: boolean
  build_failure_reason: string
  pipeline_id: number
  project_id: number
  project_name: string
  user: User
  commit: Commit
  repository: Repository
  runner: Runner
}

export interface Commit {
  id: number
  sha: string
  message: string
  author_name: string
  author_email: string
  status: string
  duration: null
  started_at: null
  finished_at: null
}

export interface Repository {
  name: string
  description: string
  homepage: string
  git_ssh_url: string
  git_http_url: string
  visibility_level: number
}

export interface Runner {
  active: boolean
  is_shared: boolean
  id: number
  description: string
}

export interface User {
  id: number
  name: string
  email: string
}
