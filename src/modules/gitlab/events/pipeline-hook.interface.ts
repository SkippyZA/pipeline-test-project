export class PipelineHook {
  object_kind: string
  object_attributes: ObjectAttributes
  merge_request: MergeRequest
  user: User
  project: Project
  commit: Commit
  builds: Build[]
}

export interface Build {
  id: number
  stage: string
  name: string
  status: string
  created_at: string
  started_at: null | string
  finished_at: null | string
  when: string
  manual: boolean
  allow_failure: boolean
  user: User
  runner: Runner | null
  artifacts_file: ArtifactsFile
}

export interface ArtifactsFile {
  filename: null
  size: null
}

export interface Runner {
  id: number
  description: string
  active: boolean
  is_shared: boolean
}

export interface User {
  name: string
  username: string
  avatar_url: string
}

export interface Commit {
  id: string
  message: string
  timestamp: Date
  url: string
  author: Author
}

export interface Author {
  name: string
  email: string
}

export interface MergeRequest {
  id: number
  iid: number
  title: string
  source_branch: string
  source_project_id: number
  target_branch: string
  target_project_id: number
  state: string
  merge_status: string
  url: string
}

export interface ObjectAttributes {
  id: number
  ref: string
  tag: boolean
  sha: string
  before_sha: string
  source: string
  status: string
  stages: string[]
  created_at: string
  finished_at: string
  duration: number
  variables: Variable[]
}

export interface Variable {
  key: string
  value: string
}

export interface Project {
  id: number
  name: string
  description: string
  web_url: string
  avatar_url: null
  git_ssh_url: string
  git_http_url: string
  namespace: string
  visibility_level: number
  path_with_namespace: string
  default_branch: string
}
