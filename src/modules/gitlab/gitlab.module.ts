import { Module } from '@nestjs/common'
import { GitlabWebhookController } from './gitlab-webhook.controller'
import { HoneycombModule } from '../honeycomb/honeycomb.module'

@Module({
  imports: [
    HoneycombModule
  ],
  controllers: [
    GitlabWebhookController
  ]
})
export class GitlabModule {}
