import { Injectable } from '@nestjs/common'
import Axios, { AxiosInstance } from 'axios'
import { compose } from 'ramda'
import { toSnakeCase } from './to-snake-case.util'
import { renameKeys } from './rename-keys.util'
import { HoneycombConfig } from './honeycomb.config'

export interface HoneycombEvent {
  name: string
  serviceName: string
  durationMs?: number
  traceId?: string
  parentId?: string
  spanId?: string
}

@Injectable()
export class HoneycombService {
  private api: AxiosInstance

  constructor (config: HoneycombConfig) {
    this.api = Axios.create({
      baseURL: `${config.host}/1`,
      headers: {
        'X-Honeycomb-Team': config.token
      },
    })
  }

  // public event (dataset: string, event HoneycombEvent) {
  public event (dataset: string, event: any, timestamp?: Date) {
    const path = `/events/${dataset}`

    const formatBody = compose(
      renameKeys({ trace_id: 'trace.trace_id', span_id: 'trace.span_id', parent_id: 'trace.parent_id' }),
      toSnakeCase,
    )

    let headers
    if (timestamp) {
      headers = {
        'X-Honeycomb-Event-Time': timestamp.valueOf() / 1000
      }
    }

    return this.api.post(path, formatBody(event), { headers })
  }
}
