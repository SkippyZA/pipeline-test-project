import { concat, isEmpty, keys, curry, map, values, is, zipObj, __, unary } from 'ramda'

const snakeCase = (str: string) => str.replace(/([A-Z])/g, x => concat('_', x.toLowerCase()))

const parseValues = curry((fn, obj) => {
  if (isEmpty(keys(obj))) {
    return obj
  } else {
    return mapKeys(fn, obj)
  }
})
const mapKeys = curry((fn, obj) => zipObj(map(fn, keys(obj)), map(parseValues(fn), values(obj))));
const fold = curry((fn, value) => is(Array, value) ? map(fn, value) : fn(value))

export const toSnakeCase = unary(fold(mapKeys(snakeCase), __))
