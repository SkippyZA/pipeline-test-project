import { Module } from '@nestjs/common'
import { HoneycombService } from './honeycomb.service'
import { ConfigModule } from '../config/config.module'
import { HoneycombConfig } from './honeycomb.config'

@Module({
  imports: [ ConfigModule.forClass(HoneycombConfig) ],
  providers: [ HoneycombService ],
  exports: [ HoneycombService ]
})
export class HoneycombModule {}
