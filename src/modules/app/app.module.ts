import { Module } from '@nestjs/common'
import { HoneycombModule } from '../honeycomb/honeycomb.module'
import { GitlabModule } from '../gitlab/gitlab.module'

@Module({
  imports: [
    HoneycombModule,
    GitlabModule
  ],
})
export class AppModule {}
