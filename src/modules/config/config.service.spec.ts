const testConfig = {
  name: 'hello world',
  example: 'this is an example config value',
  someNumber: 1000,
}

process.env.NODE_CONFIG = JSON.stringify({ test: testConfig })

import config from 'config'
import { ConfigService, FactoryMethod } from './config.service'
import { expect } from 'chai'

class TestConfig {
  name: string
  example: string
  someNumber: number
}

describe('ConfigService', () => {
  let configService: ConfigService

  beforeEach(() => {
    configService = new ConfigService(config)
  })

  it('should return a populated config object', () => {
    const configObj = configService.forClass(TestConfig)

    expect(configObj).to.be.instanceOf(TestConfig)
    expect(configObj).to.deep.equal({
      name: 'hello world',
      example: 'this is an example config value',
      someNumber: 1000,
    })
  })

  it('should use the factory to construct the config object', () => {
    const testFactory: FactoryMethod<TestConfig> = () => {
      return {
        name: 'hello',
        example: 'world',
        someNumber: 1,
      }
    }

    const configObj = configService.forClass(TestConfig, testFactory)

    expect(configObj).to.be.instanceOf(TestConfig)
    expect(configObj).to.deep.equal({
      name: 'hello',
      example: 'world',
      someNumber: 1,
    })
  })
})
