import config from 'config'

export const CONFIG = 'ConfigToken'
export const configProvider = {
  provide: CONFIG,
  useValue: config,
}
