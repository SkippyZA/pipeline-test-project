import { Module, DynamicModule } from "@nestjs/common"
import { ClassType } from 'class-transformer/ClassTransformer'
import { ConfigService, FactoryMethod } from './config.service'
import { configProvider } from './config.provider'

@Module({
  providers: [
    configProvider,
    ConfigService,
  ],
})
export class ConfigModule {
  public static forClass<T extends Record<string, any>> (cls: ClassType<T>, factory?: FactoryMethod<T>): DynamicModule {
    return {
      module: ConfigModule,
      providers: [
        {
          provide: cls,
          useFactory: (configService: ConfigService): T => configService.forClass(cls, factory),
          inject: [ ConfigService ],
        },
      ],
      exports: [
        cls,
      ],
    }
  }
}
