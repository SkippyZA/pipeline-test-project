import { ClassType } from 'class-transformer/ClassTransformer'
import { plainToClassFromExist } from 'class-transformer'
import { IConfig } from 'config'
import { Inject, Injectable } from '@nestjs/common'
import { CONFIG } from './config.provider'

export type FactoryMethod<T> = (property: string, config: IConfig) => T

@Injectable()
export class ConfigService {
  constructor (@Inject(CONFIG) private config: IConfig) {}

  forClass<T extends Record<string, any>> (cls: ClassType<T>, factory?: FactoryMethod<T>): T {
    const o = new cls()
    const property = ConfigService.configPropertyFromClassName(o.constructor.name)
    let configForType: T

    if (factory) {
      configForType = factory(property, this.config)
    } else {
      configForType = this.config.get(property) as T
    }

    return plainToClassFromExist(o, configForType)
  }

  private static configPropertyFromClassName (str: string): string {
    const parts =  str.replace(/([a-z])([A-Z])/g, '$1 $2').split(' ')
    parts[0] = parts[0].toLowerCase()

    if (parts.slice(-1)[0] === 'Config') {
      parts.pop()
    }

    return parts.join('')
  }

}
