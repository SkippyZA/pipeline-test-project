# Config Module

## Usage

### Basic Usage

*config.json*
```json
{
  "app": {
    "name": "example",
    "port": 3000,
    "logLevel": "info"
  }
}
```

*Implementation*
```typescript
// app.config.ts
export class AppConfig {
  name: string
  port: number
  logLevel: string
}

// app.service.ts
@Injectable()
class AppService {
  constructor (private config: AppConfig) {}

  printConfig () {
    console.log(this.config)
    // Output = {
    //   name: 'example',
    //   port: 3000,
    //   logLevel: 'info'
    // }
  }
}

// app.module.ts
@Module({
  imports: [
    ConfigModule.forClass(AppConfig)
  ],
  providers: [
    AppService
  ]
})
class AppModule {}
```


### Configuration Factory

#### config.json
```json
{
  "app": {
    "name": "example",
    "port": 3000
  },
  "logger": {
    "level": "info"
  }
}
```

#### Implementation
```typescript
// app.config.ts
export class AppConfig {
  name: string
  port: number
  logLevel: string
}

// app-config.factroy.ts
class AppConfigFactory {
  static createConfig (property: string, config: IConfig): AppConfig {
    const { name, port } = config.get('property')
    const logLevel = config.get('logger.level')

    return { name, port, logLevel }
  }
}

// app.service.ts
@Injectable()
class AppService {
  constructor (private config: AppConfig) {}

  printConfig () {
    console.log(this.config)
    // Output = {
    //   name: 'example',
    //   port: 3000,
    //   logLevel: 'info'
    // }
  }
}

// app.module.ts
@Module({
  imports: [
    ConfigModule.forClass(AppConfig, AppConfigFactory.createConfig)
  ],
  providers: [
    AppService
  ]
})
class AppModule {}
```
